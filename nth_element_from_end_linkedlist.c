/* Return nth node from the end of the Linked list*/

#include <stdio.h>
#include <stdlib.h>

typedef struct node
{
	int value;
	struct node* next;
}node;

void add_list(node** head, node** tail, int value)
{
	node* temp1, * temp2;
	temp1 = (node*)malloc(sizeof(struct node));
	temp1->value = value;
	temp1->next = NULL;

	if (*head == NULL)
	{
		*head = temp1;
		*tail = temp1;
	}
	else
	{
		for (temp2 = *head; temp2->next != NULL; temp2 = temp2->next);
		temp2->next = temp1;
		*tail = temp1;
	}
}

void print_list(node* head)
{
	node *temp;
	for (temp = head; temp != NULL; temp = temp->next)
	{
		printf("[%d]->", temp->value);
	}
	printf("[NULL]\n");
}

node* nth_node(node* head, int n)
{
	node *temp1, *temp2;
	int count = 0;

	if (head == NULL)
	{
		return NULL;
	}

	temp1 = head;
	temp2 = head;

	//Number of nodes are less than n
	while (count < n)
	{
		if (temp1->next == NULL)
		{
			return NULL;
		}
		temp1 = temp1->next;
		++count;
	}

	while (temp1 != NULL)
	{
		temp1 = temp1->next;
		temp2 = temp2->next;
	}
	return temp2;
}

int main()
{
	node *head, *tail;
	head = NULL;

	add_list(&head, &tail, 1);
	add_list(&head, &tail, 2);
	add_list(&head, &tail, 3);
	add_list(&head, &tail, 4);
	add_list(&head, &tail, 5);
	add_list(&head, &tail, 5);
	add_list(&head, &tail, 7);
	add_list(&head, &tail, 8);
	add_list(&head, &tail, 9);
	print_list(head);

	node* result;
	result = nth_node(head, 6);
	printf("Nth node from end is : %d", result->value);
	return 0;
}