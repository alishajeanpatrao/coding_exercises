/* String Compression
   Input : str_input = {'a','a','b','b','c','c','c'}
   Modified String : str_input = {'a','2','b','2','c','3'}
   Output : Length of String: 6
*/

#include <iostream>
#include <vector>

using namespace std;

int string_compression(vector<char> &str_input)
{
    int write = 0;
    for(int iter=1, count=1; iter<=str_input.size(); iter++,count++)
    {
        if(iter==str_input.size() || str_input[iter]!=str_input[iter-1])
        {
            str_input[write++] = str_input[iter-1];
            if(count>=2)
            {
                for(auto c : to_string(count))
                {
                    str_input[write++] = c;
                }
            }
            count  = 0;
        }
    }
	return write;	
}

int main()
{
	std::vector<char>str_input = {'a','a','b','b','c','c','c'};
	int total_elements = string_compression(str_input);
	cout << "Total size of the compressed string" << total_elements << endl;
	return 0;
}