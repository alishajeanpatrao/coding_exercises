/*Reverse a Linked list - Iterative Method*/

#include <stdio.h>
#include <stdlib.h>

//Structure for a node
typedef struct node
{
	int value;
	struct node* next;
}node;

//Global Variables
node *head, *temp, *tail;

//Adding elements to the Linked list
void add_list(int value)
{
	temp = (node*)malloc(sizeof(struct node));
	temp->value = value;
	temp->next = (node*)0;


	if (head == (node*)0)
	{
		head = temp;
		tail = temp;
	}
	else
	{
		tail->next = temp;
		tail = temp;
	}
}

//Print elements of Linked list
void print_list()
{
	if (head == (node*)0)
	{
		return;
	}
	for (temp = head; temp != (node*)0; temp = temp->next)
	{
		printf("[%d]->", temp->value);
	}
	printf("[NULL]\n");
}

//Reverse elements of Linked list
void iterative_reverse()
{
	node *p, *q, *r;
	if (head == (node*)0)
	{
		return;
	}
	p = head;
	q = p->next;
	p->next = (node*)0;

	while (q != (node*)0)
	{
		r = q->next;
		q->next = p;
		p = q;
		q = r;
	}
	head = p;
}

int main()
{
	add_list(1);
	add_list(2);
	add_list(3);
	add_list(4);
	print_list();

	iterative_reverse();
	print_list();
	return 0;
}



