/* Create a copy of the linked list*/

#include <stdio.h>
#include <stdlib.h>

typedef struct node
{
	int value;
	struct node* next;
}node;

void add_list(node** head, node** tail, int val)
{
	node* forward, * seeking;
	seeking = (node*)malloc(sizeof(struct node));
	seeking->value = val;
	seeking->next = NULL;
	
	if (*head == NULL)
	{
		*head = seeking;
		*tail = seeking;
	}
	else
	{
		for (forward = *head; forward->next != NULL; forward = forward->next);
		forward->next = seeking;
		*tail = seeking;
	}
}

void print_list(node* head)
{
	node* iter;
	for (iter = head; iter != NULL; iter = iter->next)
	{
		printf("[%d]", iter->value);
	}
	printf("[NULL]\n");
}

node* copy_linkedlist(node* head)
{
	node *copy_ptr, *orig_ptr;
	orig_ptr = head;
	copy_ptr = (node*)malloc(sizeof(struct node));

	if(orig_ptr== NULL)
	{ 
		return NULL;
	}
	copy_ptr->value = orig_ptr->value;
	copy_ptr->next = copy_linkedlist(orig_ptr->next);

	return copy_ptr;	
}


int main()
{
	node *head, *tail;
	head = (node*)0;
	
	add_list(&head, &tail, 1);
	add_list(&head, &tail, 2);
	add_list(&head, &tail, 3);
	add_list(&head, &tail, 4);
	add_list(&head, &tail, 5);
	print_list(head);

	node* new_copy = copy_linkedlist(head);
	print_list(new_copy);
	return 0;
}