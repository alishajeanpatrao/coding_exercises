#include <stdio.h>
#include <stdbool.h>

bool is_little_endian()
{
    int val = 0x56781234;
    char *ptr= (char*)&val;
    bool answer;
    if((*ptr)==0x34)
    {
        answer = true;
    }
    else
    {
        answer = false;
    }
    return answer;
}

int main()
{
    bool res = is_little_endian();
    if(res)
    {
        printf("Little Endian \n");
    }
    else
    {
        printf("Big Endian \n");
    }
    return 0;
}