/*Reverse a linked list - Recursive method*/

#include<stdio.h>
#include<stdlib.h>

typedef struct node
{
	int value;
	struct node* next;
}node;

node *head, *tail, *temp;

void add_list(int value)
{
	temp = (node*) malloc(sizeof(struct node));
	temp->value = value;
	temp->next = (node*)0;

	if (head == (node*)0)
	{
		head = temp;
		tail = temp;
	}
	else
	{
		tail->next = temp;
		tail = temp;
	}
}

void print_list()
{
	if (head == (node*)0)
	{
		return;
	}

	for (temp = head; temp != (node*)0; temp = temp->next)
	{
		printf("[%d]->", temp->value);
	}
	printf("[NULL]\n");
}

node* recursion_reverse(node* root)
{
	if (root->next != (node*)0)
	{
		recursion_reverse(root->next);
		root->next->next = root;
		return(root);
	}
	else
	{
		head = root;
	}
}

int main()
{
	head = (node*)0;
	add_list(1);
	add_list(2);
	add_list(3);
	add_list(4);
	print_list();

	if (head->next != (node*)0)
	{
		tail = recursion_reverse(head);
		tail->next = (node*)0;
	}
	print_list();

	return 0;
}